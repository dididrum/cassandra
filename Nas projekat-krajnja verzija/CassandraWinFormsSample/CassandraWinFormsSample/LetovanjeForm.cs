﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CassandraDataLayer;
using CassandraDataLayer.QueryEntities;

namespace CassandraWinFormsSample
{
    public partial class LetovanjeForm : Form
    {
        DataProvider db = new DataProvider();

        int brojac = 5;
        
        public LetovanjeForm()
        {
            InitializeComponent();
        }

        private void Letovanje_Click(object sender, EventArgs e)
        {
            List<Letovanje> letovanja = DataProvider.GetLetovanja();

            foreach (Letovanje l in letovanja)
            {
                MessageBox.Show(l.datum + " " + l.lokacija);
            }
        }

        private void Putovanja_Click(object sender, EventArgs e)
        {
            List<Putovanja> putovanja = DataProvider.GetPutovanja();

            foreach (Putovanja p in putovanja)
            {
                MessageBox.Show(p.datum + " " + p.lokacija);
            }    
                
        }

        private void Zimovanje_Click(object sender, EventArgs e)
        {
            List<Zimovanje> zimovanja = DataProvider.GetZimovanja();

            foreach (Zimovanje z in zimovanja)
            {
                MessageBox.Show(z.datum + " " + z.lokacija);
                
            }
        }

        private void addHotelButton_Click(object sender, EventArgs e)
        {
            
            
        }

        private void datumm_TextChanged(object sender, EventArgs e)
        {

        }

      
      

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void dodajLetovanjeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(lokacijaletovanja.Text) || string.IsNullOrWhiteSpace(datumletovanja.Text))
            {
                MessageBox.Show("Niste uneli sve podatke.");
                return;
            }


            string broj = Convert.ToString(brojac);

            db.AddLetovanje(broj, datumletovanja.Text, lokacijaletovanja.Text);

            datumletovanja.Text = "";
            lokacijaletovanja.Text = "";

            this.brojac++;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dodajZimovanjeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(lokacijazimovanja.Text) || string.IsNullOrWhiteSpace(datumzimovanja.Text))
            {
                MessageBox.Show("Niste uneli sve podatke.");
                return;
            }


            string broj = Convert.ToString(brojac);

            db.AddZimovanje(broj, datumzimovanja.Text, lokacijazimovanja.Text);

            datumzimovanja.Text = "";
            lokacijazimovanja.Text = "";

            this.brojac++;
        }

      

        private void dodajPutovanjeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(lokacijaputovanja.Text) || string.IsNullOrWhiteSpace(datumputovanja.Text))
            {
                MessageBox.Show("Niste uneli sve podatke.");
                return;
            }


            string broj = Convert.ToString(brojac);

            db.AddPutovanja(broj, datumputovanja.Text, lokacijaputovanja.Text);

            datumputovanja.Text = "";

            lokacijaputovanja.Text = "";

            this.brojac++;
        }

      
    }
}
