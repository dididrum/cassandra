﻿namespace CassandraWinFormsSample
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.addHotelButton = new System.Windows.Forms.Button();
            this.Hotel = new System.Windows.Forms.GroupBox();
            this.datum = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.datumm = new System.Windows.Forms.TextBox();
            this.naziv = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button8 = new System.Windows.Forms.Button();
            this.numericUpDownAktivnosti = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.ToDoList = new System.Windows.Forms.ListBox();
            this.deleteHotelButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.opis = new System.Windows.Forms.TextBox();
            this.Room = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.Hotel.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAktivnosti)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.Room.SuspendLayout();
            this.SuspendLayout();
            // 
            // addHotelButton
            // 
            this.addHotelButton.Location = new System.Drawing.Point(183, 158);
            this.addHotelButton.Name = "addHotelButton";
            this.addHotelButton.Size = new System.Drawing.Size(120, 23);
            this.addHotelButton.TabIndex = 1;
            this.addHotelButton.Text = "Dodaj aktivnost";
            this.addHotelButton.UseVisualStyleBackColor = true;
            this.addHotelButton.Click += new System.EventHandler(this.addHotelButton_Click);
            // 
            // Hotel
            // 
            this.Hotel.BackColor = System.Drawing.Color.LavenderBlush;
            this.Hotel.Controls.Add(this.button9);
            this.Hotel.Controls.Add(this.datum);
            this.Hotel.Controls.Add(this.label7);
            this.Hotel.Controls.Add(this.label5);
            this.Hotel.Controls.Add(this.datumm);
            this.Hotel.Controls.Add(this.naziv);
            this.Hotel.Controls.Add(this.groupBox2);
            this.Hotel.Controls.Add(this.groupBox1);
            this.Hotel.Controls.Add(this.label4);
            this.Hotel.Controls.Add(this.opis);
            this.Hotel.Controls.Add(this.Room);
            this.Hotel.Controls.Add(this.label1);
            this.Hotel.Controls.Add(this.addHotelButton);
            this.Hotel.Location = new System.Drawing.Point(53, 12);
            this.Hotel.Name = "Hotel";
            this.Hotel.Size = new System.Drawing.Size(606, 451);
            this.Hotel.TabIndex = 2;
            this.Hotel.TabStop = false;
            // 
            // datum
            // 
            this.datum.AutoSize = true;
            this.datum.Location = new System.Drawing.Point(72, 50);
            this.datum.Name = "datum";
            this.datum.Size = new System.Drawing.Size(44, 13);
            this.datum.TabIndex = 19;
            this.datum.Text = "Datum: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(72, 129);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Opis:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(72, 86);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Naziv:";
            // 
            // datumm
            // 
            this.datumm.Location = new System.Drawing.Point(122, 50);
            this.datumm.Margin = new System.Windows.Forms.Padding(2);
            this.datumm.Multiline = true;
            this.datumm.Name = "datumm";
            this.datumm.Size = new System.Drawing.Size(101, 22);
            this.datumm.TabIndex = 16;
            // 
            // naziv
            // 
            this.naziv.Location = new System.Drawing.Point(122, 79);
            this.naziv.Margin = new System.Windows.Forms.Padding(2);
            this.naziv.Multiline = true;
            this.naziv.Name = "naziv";
            this.naziv.Size = new System.Drawing.Size(99, 29);
            this.naziv.TabIndex = 15;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button8);
            this.groupBox2.Controls.Add(this.numericUpDownAktivnosti);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.ToDoList);
            this.groupBox2.Controls.Add(this.deleteHotelButton);
            this.groupBox2.Location = new System.Drawing.Point(309, 50);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(281, 137);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(225, 7);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(46, 30);
            this.button8.TabIndex = 14;
            this.button8.Text = "Ucitaj";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // numericUpDownAktivnosti
            // 
            this.numericUpDownAktivnosti.Location = new System.Drawing.Point(102, 14);
            this.numericUpDownAktivnosti.Name = "numericUpDownAktivnosti";
            this.numericUpDownAktivnosti.Size = new System.Drawing.Size(38, 20);
            this.numericUpDownAktivnosti.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Odaberi aktivnost";
            // 
            // ToDoList
            // 
            this.ToDoList.FormattingEnabled = true;
            this.ToDoList.Location = new System.Drawing.Point(6, 36);
            this.ToDoList.Name = "ToDoList";
            this.ToDoList.Size = new System.Drawing.Size(242, 95);
            this.ToDoList.TabIndex = 11;
            // 
            // deleteHotelButton
            // 
            this.deleteHotelButton.Location = new System.Drawing.Point(173, 7);
            this.deleteHotelButton.Name = "deleteHotelButton";
            this.deleteHotelButton.Size = new System.Drawing.Size(46, 30);
            this.deleteHotelButton.TabIndex = 3;
            this.deleteHotelButton.Text = "Obrisi ";
            this.deleteHotelButton.UseVisualStyleBackColor = true;
            this.deleteHotelButton.Click += new System.EventHandler(this.deleteHotelButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Location = new System.Drawing.Point(67, 193);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(484, 74);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Moje uspomene";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(114, 31);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(87, 22);
            this.button6.TabIndex = 5;
            this.button6.Text = "Odmor ";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(288, 31);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 22);
            this.button7.TabIndex = 6;
            this.button7.Text = "Uspomene";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(207, 31);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 22);
            this.button5.TabIndex = 4;
            this.button5.Text = "Fakultet";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Thistle;
            this.label4.Location = new System.Drawing.Point(393, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "TO DO list";
            // 
            // opis
            // 
            this.opis.Location = new System.Drawing.Point(122, 112);
            this.opis.Margin = new System.Windows.Forms.Padding(2);
            this.opis.Multiline = true;
            this.opis.Name = "opis";
            this.opis.Size = new System.Drawing.Size(179, 41);
            this.opis.TabIndex = 10;
            // 
            // Room
            // 
            this.Room.Controls.Add(this.label3);
            this.Room.Controls.Add(this.label2);
            this.Room.Controls.Add(this.button4);
            this.Room.Controls.Add(this.button3);
            this.Room.Controls.Add(this.button1);
            this.Room.Controls.Add(this.button2);
            this.Room.Location = new System.Drawing.Point(67, 273);
            this.Room.Name = "Room";
            this.Room.Size = new System.Drawing.Size(484, 163);
            this.Room.TabIndex = 3;
            this.Room.TabStop = false;
            this.Room.Text = "Saveti ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS PGothic", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(455, 60);
            this.label3.TabIndex = 5;
            this.label3.Text = resources.GetString("label3.Text");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 4;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Thistle;
            this.button4.Location = new System.Drawing.Point(378, 28);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(88, 40);
            this.button4.TabIndex = 8;
            this.button4.Text = "TRENING fitness";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Thistle;
            this.button3.Location = new System.Drawing.Point(261, 28);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(88, 40);
            this.button3.TabIndex = 7;
            this.button3.Text = "NEGA beauty";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Thistle;
            this.button1.Location = new System.Drawing.Point(18, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 40);
            this.button1.TabIndex = 5;
            this.button1.Text = "MODA trend";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Thistle;
            this.button2.Location = new System.Drawing.Point(137, 28);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(97, 40);
            this.button2.TabIndex = 6;
            this.button2.Text = "ZDRAVLJE lepota";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bradley Hand ITC", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(209, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(207, 26);
            this.label1.TabIndex = 4;
            this.label1.Text = "ORGANIZUJ SEBE";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(133, 158);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(46, 23);
            this.button9.TabIndex = 20;
            this.button9.Text = "Izmeni";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(707, 488);
            this.Controls.Add(this.Hotel);
            this.Name = "mainForm";
            this.Text = "Organizator";
            this.Hotel.ResumeLayout(false);
            this.Hotel.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAktivnosti)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.Room.ResumeLayout(false);
            this.Room.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button addHotelButton;
        private System.Windows.Forms.GroupBox Hotel;
        private System.Windows.Forms.Button deleteHotelButton;
        private System.Windows.Forms.GroupBox Room;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox opis;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox ToDoList;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown numericUpDownAktivnosti;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label datum;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox datumm;
        private System.Windows.Forms.TextBox naziv;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
    }
}

