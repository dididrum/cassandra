﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CassandraWinFormsSample
{
    public partial class NegaForm : Form
    {
        public NegaForm()
        {
            InitializeComponent();
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void negaDugme1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Čarolija jogurta deluje mnogostruko na razne probleme sa kožom, a pogotovo na podočnjake! Tajna je zapravo u kombinaciji sastojaka koji deluju na umanjivanje otoka ispod očiju i tamnih krugova podočnjaka. Kalcijum  je jedan od najpoznatijih sastojaka mleka, a samim tim i jogurta! On hrani kožu i pomaže joj da se regeneriše i pomaže da se izlučuje višak vode koji se nakuplja u podočnjacima i izaziva otok.Kalcijum iz jogurta je dakle zadužen za smanjivanje nadutih podočnjaka. ");
        }

        private void negaDugme2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Maska od gline za lice je fantastična ukoliko želite da smanjite upalu na licu i održite kožu mladolikom. Naučite da je  pripremite pravilno kako bi uživali u njenim blagodetima. ");
        }

        private void negaDugme3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Oslanjanje na prirodna sredstva kada je lečenje bubuljica na licu u pitanju, nije ništa novo. U prirodi se nalaze izuzetno lekoviti sastojci, koji mogu da pomognu kod većine problema sa kožom. Jedna od lekovitih biljaka koja se izdvaja je – žuti noćurak.");
        }

    }
}
