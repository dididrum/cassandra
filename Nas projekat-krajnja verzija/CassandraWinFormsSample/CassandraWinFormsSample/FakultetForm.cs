﻿using System;
using CassandraDataLayer;
using CassandraDataLayer.QueryEntities;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CassandraWinFormsSample
{
    public partial class FakultetForm : Form
    {
        DataProvider db = new DataProvider();
        List<CheckBox> lista = new List<CheckBox>();
        int brojac = 5;
        public FakultetForm()
        {
            InitializeComponent();
            ucitajPodatke();
           
        }

        private void addHotelButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(opis.Text) || string.IsNullOrWhiteSpace(datumm.Text))
            {
                MessageBox.Show("Niste uneli sve podatke.");
                return;
            }

            //Aktivnosti akt = new Aktivnosti();
            //akt.datum = datumm.Text;
            //akt.naziv = naziv.Text;
            //akt.opis = opis.Text;
            string broj = Convert.ToString(brojac);
            db.AddFakultet(broj, datumm.Text, opis.Text, "nije");

            datumm.Text = "";
            opis.Text = "";

            this.brojac++;
            ucitajPodatke();
        }

        private void ucitajPodatke()
        {
            flowLayoutPanel1.Controls.Clear();
           List<Fakultet> fakulteti = db.GetFakulteti();

            //ToDoList.DataSource = null;

            List<string> proba = new List<string>();
           

            foreach (var test in fakulteti)
            {
                CheckBox box= new CheckBox();
                box.Tag = test.rbobaveze;
                box.Width = 200;
                box.Text = test.rbobaveze.ToString() + ".  " + test.datum.ToString() + " - " + test.opis.ToString();
                box.CheckedChanged += new EventHandler(obrisiObavezu);
                lista.Add(box);
                //box.Checked = true;
                // else
                // box.Checked = false;

                flowLayoutPanel1.Controls.Add(box);
                //ispitaj(box, test.uradjen);

            }

           
            //ToDoList.DataSource = proba;
        }

        private void ispitaj(CheckBox dugme, string zadatak)
        {
            if (dugme.Checked == true)
                zadatak = "uradjen";
            
            if (zadatak == "uradjen")
                dugme.Checked = true;
            else
                dugme.Checked = false;

        }

        private void obrisiObavezu(object sender, EventArgs e)
        {
            CheckBox dugme = sender as CheckBox;
            if (dugme.Checked)
            {
                db.DeleteFakultet(dugme.Tag.ToString());
                MessageBox.Show("Obrisana obaveza!");
            }
            ucitajPodatke();
        }

        
    }


}
