﻿using CassandraDataLayer;
using CassandraDataLayer.QueryEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CassandraWinFormsSample
{
    public partial class mainForm : Form
    {
        DataProvider db = new DataProvider();
        int brojac = 5;
        public mainForm()
        {
            InitializeComponent();

            ucitajPodatke();

        }

     
        private void ucitajPodatke()
        {
            List<Aktivnosti> aktivnosti = db.GetAktivnosti();

            ToDoList.DataSource = null;

            List<string> proba = new List<string>();


            foreach (var test in aktivnosti)
            {
                proba.Add(test.rbaktivnosti + ".  " + test.datum + " " + test.naziv + " - " + test.opis);


            }
            ToDoList.DataSource = proba;
        }


        private void addHotelButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(opis.Text) || string.IsNullOrWhiteSpace(datumm.Text))
            {
                MessageBox.Show("Niste uneli sve podatke.");
                return;
            }

            //Aktivnosti akt = new Aktivnosti();
            //akt.datum = datumm.Text;
            //akt.naziv = naziv.Text;
            //akt.opis = opis.Text;
            string broj = Convert.ToString(brojac);
            db.AddAktivnost(broj, datumm.Text, naziv.Text, opis.Text);
            datumm.Text = "";
            naziv.Text = "";
            opis.Text = "";

            this.brojac++;
            ucitajPodatke();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            ModaForm modaForm = new ModaForm();
            modaForm.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ZdravljeForm zdravljeForm = new ZdravljeForm();
            zdravljeForm.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            NegaForm negaForm = new NegaForm();
            negaForm.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            TreningForm treningForm = new TreningForm();
            treningForm.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            LetovanjeForm letovanjeForm = new LetovanjeForm();
            letovanjeForm.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FakultetForm prijateljiForm = new FakultetForm();
            prijateljiForm.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            IzlasciForm izlasciForm = new IzlasciForm();
            izlasciForm.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Aktivnosti akt = new Aktivnosti();
            string pom = Convert.ToString((int)numericUpDownAktivnosti.Value);
            akt = db.GetAktivnost(pom);

            if (akt != null)
            {
                datumm.Text = akt.datum;
                naziv.Text = akt.naziv;
                opis.Text = akt.opis; 

            }
        }

        private void deleteHotelButton_Click(object sender, EventArgs e)
        {
            string pom = Convert.ToString((int)numericUpDownAktivnosti.Value);
            db.DeleteAktivnost(pom);
            ucitajPodatke();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            string pom = Convert.ToString((int)numericUpDownAktivnosti.Value);
            string opis2 = opis.Text;
            db.UpdateAktivnost(pom, opis2);
            ucitajPodatke();
        }
    }
}
