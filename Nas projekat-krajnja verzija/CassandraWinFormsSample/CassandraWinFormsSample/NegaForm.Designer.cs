﻿
namespace CassandraWinFormsSample
{
    partial class NegaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NegaForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.negaDugme2 = new System.Windows.Forms.Button();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.negaDugme1 = new System.Windows.Forms.Button();
            this.negaDugme3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("groupBox1.BackgroundImage")));
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox1.Location = new System.Drawing.Point(34, 96);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 128);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("groupBox2.BackgroundImage")));
            this.groupBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox2.Location = new System.Drawing.Point(569, 96);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(208, 128);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MV Boli", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(594, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(200, 32);
            this.label3.TabIndex = 12;
            this.label3.Text = "Najmoćnije maske za zatezanje \r\n        vrata su pred vama! \r\n";
            // 
            // negaDugme2
            // 
            this.negaDugme2.BackColor = System.Drawing.Color.RosyBrown;
            this.negaDugme2.Font = new System.Drawing.Font("MV Boli", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.negaDugme2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.negaDugme2.Location = new System.Drawing.Point(549, 79);
            this.negaDugme2.Name = "negaDugme2";
            this.negaDugme2.Size = new System.Drawing.Size(89, 30);
            this.negaDugme2.TabIndex = 8;
            this.negaDugme2.Text = "Saznaj vise\r\n";
            this.negaDugme2.UseVisualStyleBackColor = false;
            this.negaDugme2.Click += new System.EventHandler(this.negaDugme2_Click);
            // 
            // groupBox
            // 
            this.groupBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("groupBox.BackgroundImage")));
            this.groupBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox.Location = new System.Drawing.Point(491, 291);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(200, 128);
            this.groupBox.TabIndex = 5;
            this.groupBox.TabStop = false;
            this.groupBox.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // negaDugme1
            // 
            this.negaDugme1.BackColor = System.Drawing.Color.RosyBrown;
            this.negaDugme1.Font = new System.Drawing.Font("MV Boli", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.negaDugme1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.negaDugme1.Location = new System.Drawing.Point(220, 79);
            this.negaDugme1.Name = "negaDugme1";
            this.negaDugme1.Size = new System.Drawing.Size(87, 31);
            this.negaDugme1.TabIndex = 7;
            this.negaDugme1.Text = "Saznaj vise\r\n";
            this.negaDugme1.UseVisualStyleBackColor = false;
            this.negaDugme1.Click += new System.EventHandler(this.negaDugme1_Click);
            // 
            // negaDugme3
            // 
            this.negaDugme3.BackColor = System.Drawing.Color.RosyBrown;
            this.negaDugme3.Font = new System.Drawing.Font("MV Boli", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.negaDugme3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.negaDugme3.Location = new System.Drawing.Point(672, 274);
            this.negaDugme3.Name = "negaDugme3";
            this.negaDugme3.Size = new System.Drawing.Size(96, 28);
            this.negaDugme3.TabIndex = 9;
            this.negaDugme3.Text = "Saznaj vise";
            this.negaDugme3.UseVisualStyleBackColor = false;
            this.negaDugme3.Click += new System.EventHandler(this.negaDugme3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MV Boli", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(575, 255);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(193, 16);
            this.label1.TabIndex = 10;
            this.label1.Text = "Probajte ulje noćurka za akne!";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MV Boli", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(76, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(203, 32);
            this.label2.TabIndex = 11;
            this.label2.Text = "Jogurt za podočnjake je super i\r\n     evo kako da ga koristite!\r\n";
            // 
            // NegaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PeachPuff;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(806, 473);
            this.Controls.Add(this.negaDugme2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.negaDugme1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.negaDugme3);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "NegaForm";
            this.Text = "NegaForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox;
        private System.Windows.Forms.Button negaDugme1;
        private System.Windows.Forms.Button negaDugme2;
        private System.Windows.Forms.Button negaDugme3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}