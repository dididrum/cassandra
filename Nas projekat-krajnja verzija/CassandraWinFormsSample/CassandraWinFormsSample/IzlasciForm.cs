﻿using System;
using CassandraDataLayer;
using CassandraDataLayer.QueryEntities;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CassandraWinFormsSample
{
    public partial class IzlasciForm : Form
    {
        DataProvider db = new DataProvider();
        int brojac = 5;
        public IzlasciForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(opis.Text) || string.IsNullOrWhiteSpace(dat.Text))
            {
                MessageBox.Show("Niste uneli sve podatke.");
                return;
            }

            string broj = Convert.ToString(brojac);
            db.AddUspomenu(broj, dat.Text, opis.Text);
            dat.Text = "";
    
            opis.Text = "";

            this.brojac++;
            ucitajPodatke();
        }

        private void ucitajPodatke()
        {
            List<Uspomene> uspomene = db.GetUspomene();

            lista.DataSource = null;

            List<string> proba = new List<string>();


            foreach (var test in uspomene)
            {
                proba.Add(test.rbuspomene + ".  " + test.datum + " "  + test.opis);


            }
           lista.DataSource = proba;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Uspomene u = DataProvider.GetUspomenu("1");
            MessageBox.Show(u.opis);
        }

       
    }
}
