﻿
namespace CassandraWinFormsSample
{
    partial class LetovanjeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LetovanjeForm));
            this.Letovanje = new System.Windows.Forms.Button();
            this.Putovanja = new System.Windows.Forms.Button();
            this.Zimovanje = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.letovanjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datumToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.datumletovanja = new System.Windows.Forms.ToolStripTextBox();
            this.lokacija1 = new System.Windows.Forms.ToolStripMenuItem();
            this.lokacijaletovanja = new System.Windows.Forms.ToolStripTextBox();
            this.dodajLetovanjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.zimovanjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datumzimovanje = new System.Windows.Forms.ToolStripMenuItem();
            this.datumzimovanja = new System.Windows.Forms.ToolStripTextBox();
            this.lokacija2 = new System.Windows.Forms.ToolStripMenuItem();
            this.lokacijazimovanja = new System.Windows.Forms.ToolStripTextBox();
            this.dodajZimovanjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajPutovanjeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.datumToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.datumputovanja = new System.Windows.Forms.ToolStripTextBox();
            this.lokacijaa = new System.Windows.Forms.ToolStripMenuItem();
            this.lokacijaputovanja = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Letovanje
            // 
            this.Letovanje.BackColor = System.Drawing.Color.LightSkyBlue;
            this.Letovanje.Location = new System.Drawing.Point(8, 207);
            this.Letovanje.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Letovanje.Name = "Letovanje";
            this.Letovanje.Size = new System.Drawing.Size(144, 40);
            this.Letovanje.TabIndex = 15;
            this.Letovanje.Text = "Letovanje";
            this.Letovanje.UseVisualStyleBackColor = false;
            this.Letovanje.Click += new System.EventHandler(this.Letovanje_Click);
            // 
            // Putovanja
            // 
            this.Putovanja.Location = new System.Drawing.Point(240, 10);
            this.Putovanja.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Putovanja.Name = "Putovanja";
            this.Putovanja.Size = new System.Drawing.Size(144, 40);
            this.Putovanja.TabIndex = 16;
            this.Putovanja.Text = "Putovanja";
            this.Putovanja.UseVisualStyleBackColor = true;
            this.Putovanja.Click += new System.EventHandler(this.Putovanja_Click);
            // 
            // Zimovanje
            // 
            this.Zimovanje.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.Zimovanje.Location = new System.Drawing.Point(244, 12);
            this.Zimovanje.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Zimovanje.Name = "Zimovanje";
            this.Zimovanje.Size = new System.Drawing.Size(144, 40);
            this.Zimovanje.TabIndex = 17;
            this.Zimovanje.Text = "Zimovanje";
            this.Zimovanje.UseVisualStyleBackColor = false;
            this.Zimovanje.Click += new System.EventHandler(this.Zimovanje_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 20);
            this.label1.TabIndex = 18;
            // 
            // groupBox1
            // 
            this.groupBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("groupBox1.BackgroundImage")));
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox1.Controls.Add(this.Letovanje);
            this.groupBox1.Location = new System.Drawing.Point(320, 27);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(411, 257);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("groupBox2.BackgroundImage")));
            this.groupBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox2.Controls.Add(this.Putovanja);
            this.groupBox2.Location = new System.Drawing.Point(157, 347);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(422, 254);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("groupBox3.BackgroundImage")));
            this.groupBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox3.Controls.Add(this.Zimovanje);
            this.groupBox3.Location = new System.Drawing.Point(767, 292);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Size = new System.Drawing.Size(414, 263);
            this.groupBox3.TabIndex = 21;
            this.groupBox3.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("menuStrip1.BackgroundImage")));
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.zimovanjeToolStripMenuItem,
            this.toolStripMenuItem6,
            this.toolStripMenuItem8,
            this.toolStripMenuItem9,
            this.letovanjeToolStripMenuItem,
            this.toolStripMenuItem10,
            this.toolStripMenuItem11,
            this.toolStripMenuItem12,
            this.toolStripMenuItem7,
            this.toolStripMenuItem13,
            this.toolStripMenuItem14,
            this.toolStripMenuItem15,
            this.toolStripMenuItem16,
            this.toolStripMenuItem17,
            this.toolStripMenuItem18,
            this.toolStripMenuItem19,
            this.toolStripMenuItem20});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(127, 616);
            this.menuStrip1.TabIndex = 26;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // letovanjeToolStripMenuItem
            // 
            this.letovanjeToolStripMenuItem.BackColor = System.Drawing.Color.Pink;
            this.letovanjeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.datumToolStripMenuItem1,
            this.lokacija1,
            this.dodajLetovanjeToolStripMenuItem});
            this.letovanjeToolStripMenuItem.Name = "letovanjeToolStripMenuItem";
            this.letovanjeToolStripMenuItem.Size = new System.Drawing.Size(114, 29);
            this.letovanjeToolStripMenuItem.Text = "Letovanje";
            // 
            // datumToolStripMenuItem1
            // 
            this.datumToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.datumletovanja});
            this.datumToolStripMenuItem1.Name = "datumToolStripMenuItem1";
            this.datumToolStripMenuItem1.Size = new System.Drawing.Size(270, 34);
            this.datumToolStripMenuItem1.Text = "Datum";
            // 
            // datumletovanja
            // 
            this.datumletovanja.Name = "datumletovanja";
            this.datumletovanja.Size = new System.Drawing.Size(100, 31);
            // 
            // lokacija1
            // 
            this.lokacija1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lokacijaletovanja});
            this.lokacija1.Name = "lokacija1";
            this.lokacija1.Size = new System.Drawing.Size(270, 34);
            this.lokacija1.Text = "Lokacija";
            // 
            // lokacijaletovanja
            // 
            this.lokacijaletovanja.Name = "lokacijaletovanja";
            this.lokacijaletovanja.Size = new System.Drawing.Size(100, 31);
            // 
            // dodajLetovanjeToolStripMenuItem
            // 
            this.dodajLetovanjeToolStripMenuItem.Name = "dodajLetovanjeToolStripMenuItem";
            this.dodajLetovanjeToolStripMenuItem.Size = new System.Drawing.Size(270, 34);
            this.dodajLetovanjeToolStripMenuItem.Text = "Dodaj letovanje";
            this.dodajLetovanjeToolStripMenuItem.Click += new System.EventHandler(this.dodajLetovanjeToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem1.Text = "  ";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem2.Text = "  ";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem3.Text = "   ";
            // 
            // zimovanjeToolStripMenuItem
            // 
            this.zimovanjeToolStripMenuItem.BackColor = System.Drawing.Color.PeachPuff;
            this.zimovanjeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.datumzimovanje,
            this.lokacija2,
            this.dodajZimovanjeToolStripMenuItem});
            this.zimovanjeToolStripMenuItem.Name = "zimovanjeToolStripMenuItem";
            this.zimovanjeToolStripMenuItem.Size = new System.Drawing.Size(114, 29);
            this.zimovanjeToolStripMenuItem.Text = "Zimovanje";
            // 
            // datumzimovanje
            // 
            this.datumzimovanje.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.datumzimovanja});
            this.datumzimovanje.Name = "datumzimovanje";
            this.datumzimovanje.Size = new System.Drawing.Size(270, 34);
            this.datumzimovanje.Text = "Datum";
            // 
            // datumzimovanja
            // 
            this.datumzimovanja.Name = "datumzimovanja";
            this.datumzimovanja.Size = new System.Drawing.Size(100, 31);
            // 
            // lokacija2
            // 
            this.lokacija2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lokacijazimovanja});
            this.lokacija2.Name = "lokacija2";
            this.lokacija2.Size = new System.Drawing.Size(270, 34);
            this.lokacija2.Text = "Lokacija";
            // 
            // lokacijazimovanja
            // 
            this.lokacijazimovanja.Name = "lokacijazimovanja";
            this.lokacijazimovanja.Size = new System.Drawing.Size(100, 31);
            // 
            // dodajZimovanjeToolStripMenuItem
            // 
            this.dodajZimovanjeToolStripMenuItem.Name = "dodajZimovanjeToolStripMenuItem";
            this.dodajZimovanjeToolStripMenuItem.Size = new System.Drawing.Size(270, 34);
            this.dodajZimovanjeToolStripMenuItem.Text = "Dodaj zimovanje";
            this.dodajZimovanjeToolStripMenuItem.Click += new System.EventHandler(this.dodajZimovanjeToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem4.Text = "   ";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem5.Text = "   ";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem6.Text = "   ";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.BackColor = System.Drawing.Color.LightYellow;
            this.toolStripMenuItem7.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajPutovanjeToolStripMenuItem1,
            this.datumToolStripMenuItem3,
            this.lokacijaa});
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem7.Text = "   Putovanje";
            // 
            // dodajPutovanjeToolStripMenuItem1
            // 
            this.dodajPutovanjeToolStripMenuItem1.Name = "dodajPutovanjeToolStripMenuItem1";
            this.dodajPutovanjeToolStripMenuItem1.Size = new System.Drawing.Size(270, 34);
            this.dodajPutovanjeToolStripMenuItem1.Text = "Dodaj putovanje";
            this.dodajPutovanjeToolStripMenuItem1.Click += new System.EventHandler(this.dodajPutovanjeToolStripMenuItem1_Click);
            // 
            // datumToolStripMenuItem3
            // 
            this.datumToolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.datumputovanja});
            this.datumToolStripMenuItem3.Name = "datumToolStripMenuItem3";
            this.datumToolStripMenuItem3.Size = new System.Drawing.Size(270, 34);
            this.datumToolStripMenuItem3.Text = "Datum";
            // 
            // datumputovanja
            // 
            this.datumputovanja.Name = "datumputovanja";
            this.datumputovanja.Size = new System.Drawing.Size(100, 31);
            // 
            // lokacijaa
            // 
            this.lokacijaa.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lokacijaputovanja});
            this.lokacijaa.Name = "lokacijaa";
            this.lokacijaa.Size = new System.Drawing.Size(270, 34);
            this.lokacijaa.Text = "Lokacija";
            // 
            // lokacijaputovanja
            // 
            this.lokacijaputovanja.Name = "lokacijaputovanja";
            this.lokacijaputovanja.Size = new System.Drawing.Size(100, 31);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem8.Text = "   ";
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem9.Text = "  ";
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem10.Text = " ";
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem11.Text = " ";
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem12.Text = "  ";
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem13.Text = " ";
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem14.Text = "  ";
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem15.Text = " ";
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            this.toolStripMenuItem16.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem16.Text = " ";
            // 
            // toolStripMenuItem17
            // 
            this.toolStripMenuItem17.Name = "toolStripMenuItem17";
            this.toolStripMenuItem17.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem17.Text = "  ";
            // 
            // toolStripMenuItem18
            // 
            this.toolStripMenuItem18.Name = "toolStripMenuItem18";
            this.toolStripMenuItem18.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem18.Text = " ";
            // 
            // toolStripMenuItem19
            // 
            this.toolStripMenuItem19.Name = "toolStripMenuItem19";
            this.toolStripMenuItem19.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem19.Text = " ";
            // 
            // toolStripMenuItem20
            // 
            this.toolStripMenuItem20.Name = "toolStripMenuItem20";
            this.toolStripMenuItem20.Size = new System.Drawing.Size(114, 29);
            this.toolStripMenuItem20.Text = " ";
            // 
            // LetovanjeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(1223, 616);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "LetovanjeForm";
            this.Text = "LetovanjeForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Letovanje;
        private System.Windows.Forms.Button Putovanja;
        private System.Windows.Forms.Button Zimovanje;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ToolStripMenuItem letovanjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajLetovanjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zimovanjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajZimovanjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem datumToolStripMenuItem1;
        private System.Windows.Forms.ToolStripTextBox datumletovanja;
        private System.Windows.Forms.ToolStripMenuItem lokacija1;
        private System.Windows.Forms.ToolStripTextBox lokacijaletovanja;
        private System.Windows.Forms.ToolStripMenuItem datumzimovanje;
        private System.Windows.Forms.ToolStripTextBox datumzimovanja;
        private System.Windows.Forms.ToolStripMenuItem lokacija2;
        private System.Windows.Forms.ToolStripTextBox lokacijazimovanja;
        private System.Windows.Forms.ToolStripMenuItem dodajPutovanjeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem datumToolStripMenuItem3;
        private System.Windows.Forms.ToolStripTextBox datumputovanja;
        private System.Windows.Forms.ToolStripMenuItem lokacijaa;
        private System.Windows.Forms.ToolStripTextBox lokacijaputovanja;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem17;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem18;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem19;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem20;
    }
}