﻿
namespace CassandraWinFormsSample
{
    partial class FakultetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FakultetForm));
            this.label4 = new System.Windows.Forms.Label();
            this.opis = new System.Windows.Forms.TextBox();
            this.addHotelButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.datumm = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.AntiqueWhite;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label4.Location = new System.Drawing.Point(711, 32);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(169, 20);
            this.label4.TabIndex = 18;
            this.label4.Text = "Kalendar aktivnosti";
            // 
            // opis
            // 
            this.opis.BackColor = System.Drawing.Color.White;
            this.opis.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.opis.Location = new System.Drawing.Point(88, 111);
            this.opis.Multiline = true;
            this.opis.Name = "opis";
            this.opis.Size = new System.Drawing.Size(415, 119);
            this.opis.TabIndex = 16;
            // 
            // addHotelButton
            // 
            this.addHotelButton.BackColor = System.Drawing.Color.Cornsilk;
            this.addHotelButton.Location = new System.Drawing.Point(350, 238);
            this.addHotelButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addHotelButton.Name = "addHotelButton";
            this.addHotelButton.Size = new System.Drawing.Size(180, 49);
            this.addHotelButton.TabIndex = 13;
            this.addHotelButton.Text = "Dodaj obaveze";
            this.addHotelButton.UseVisualStyleBackColor = false;
            this.addHotelButton.Click += new System.EventHandler(this.addHotelButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(108, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(358, 18);
            this.label1.TabIndex = 19;
            this.label1.Text = "Upisi datum i predstojecu aktivnost";
            // 
            // datumm
            // 
            this.datumm.BackColor = System.Drawing.Color.White;
            this.datumm.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.datumm.Location = new System.Drawing.Point(88, 65);
            this.datumm.Multiline = true;
            this.datumm.Name = "datumm";
            this.datumm.Size = new System.Drawing.Size(148, 38);
            this.datumm.TabIndex = 21;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(580, 69);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(464, 477);
            this.flowLayoutPanel1.TabIndex = 22;
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(100, 323);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 23;
            // 
            // FakultetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1200, 692);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.datumm);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.opis);
            this.Controls.Add(this.addHotelButton);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FakultetForm";
            this.Text = "Fakultet-organizacija ispita";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox opis;
        private System.Windows.Forms.Button addHotelButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox datumm;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
    }
}