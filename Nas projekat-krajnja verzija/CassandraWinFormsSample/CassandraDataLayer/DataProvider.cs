﻿using Cassandra;
using CassandraDataLayer.QueryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CassandraDataLayer
{
    public class DataProvider
    {
        

        #region Letovanje
        public static Letovanje GetLetovanje(string rbletovanja)
        {
            ISession session = SessionManager.GetSession();
            Letovanje letovanje = new Letovanje();

            if (session == null)
                return null;

            Row letovanjeData = session.Execute("select * from \"Letovanje\" where \"rbletovanja\"='1'").FirstOrDefault();

            if (letovanjeData != null)
            {
                letovanje.rbletovanja = letovanjeData["rbletovanja"] != null ? letovanjeData["rbletovanja"].ToString() : string.Empty;
                letovanje.datum = letovanjeData["datum"] != null ? letovanjeData["datum"].ToString() : string.Empty;
                letovanje.lokacija = letovanjeData["lokacija"] != null ? letovanjeData["lokacija"].ToString() : string.Empty;
             
            }

            return letovanje;
        }

        public static List<Letovanje> GetLetovanja()
        {
            ISession session = SessionManager.GetSession();
            List<Letovanje> letovanja = new List<Letovanje>();


            if (session == null)
                return null;

            var letovanjaData = session.Execute("select * from \"Letovanje\"");


            foreach (var letovanjeData in letovanjaData)
            {
                Letovanje letovanje = new Letovanje();
                letovanje.rbletovanja = letovanjeData["rbletovanja"] != null ? letovanjeData["rbletovanja"].ToString() : string.Empty;
                letovanje.datum = letovanjeData["datum"] != null ? letovanjeData["datum"].ToString() : string.Empty;
                letovanje.lokacija = letovanjeData["lokacija"] != null ? letovanjeData["lokacija"].ToString() : string.Empty;
               
                letovanja.Add(letovanje);
            }



            return letovanja;
        }

        public  void AddLetovanje(string rbletovanja, string datum, string lokacija)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;
            RowSet letovanjeData = session.Execute("insert into \"Letovanje\" (\"rbletovanja\", datum,lokacija )   values ('" + rbletovanja + "', '" + datum + "', '" + lokacija+ " ')");


        }


        #endregion

        #region Zimovanje
        public static Zimovanje GetZimovanje(string rbzimovanja)
        {
            ISession session = SessionManager.GetSession();
            Zimovanje zimovanje = new Zimovanje();

            if (session == null)
                return null;

            Row zimovanjeData = session.Execute("select * from \"Zimovanje\" where \"rbzimovanja\"='1'").FirstOrDefault();

            if (zimovanjeData != null)
            {
                zimovanje.rbzimovanja = zimovanjeData["rbzimovanja"] != null ? zimovanjeData["rbzimovanja"].ToString() : string.Empty;
                zimovanje.datum = zimovanjeData["datum"] != null ? zimovanjeData["datum"].ToString() : string.Empty;
                zimovanje.lokacija = zimovanjeData["lokacija"] != null ? zimovanjeData["lokacija"].ToString() : string.Empty;

            }

            return zimovanje;
        }

        public static List<Zimovanje> GetZimovanja()
        {
            ISession session = SessionManager.GetSession();
            List<Zimovanje> zimovanja = new List<Zimovanje>();


            if (session == null)
                return null;

            var zimovanjaData = session.Execute("select * from \"Zimovanje\"");


            foreach (var zimovanjeData in zimovanjaData)
            {
                Zimovanje zimovanje = new Zimovanje();
                zimovanje.rbzimovanja = zimovanjeData["rbzimovanja"] != null ? zimovanjeData["rbzimovanja"].ToString() : string.Empty;
                zimovanje.datum = zimovanjeData["datum"] != null ? zimovanjeData["datum"].ToString() : string.Empty;
                zimovanje.lokacija = zimovanjeData["lokacija"] != null ? zimovanjeData["lokacija"].ToString() : string.Empty;

                zimovanja.Add(zimovanje);
            }



            return zimovanja;
        }

        public  void AddZimovanje(string rbzimovanja, string datum, string lokacija)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            
            RowSet zimovanjeData = session.Execute("insert into \"Zimovanje\" (\"rbzimovanja\", datum,lokacija )   values ('" + rbzimovanja + "', '" + datum + "', '" + lokacija + " ')");
        }
        #endregion

        #region Putovanja
        public static Putovanja GetPutovanje(string rbputovanja)
        {
            ISession session = SessionManager.GetSession();
            Putovanja putovanje = new Putovanja();

            if (session == null)
                return null;

            Row putovanjeData = session.Execute("select * from \"Putovanje\" where \"rbputovanja\"='1'").FirstOrDefault();

            if (putovanjeData != null)
            {
                putovanje.rbputovanja = putovanjeData["rbputovanja"] != null ? putovanjeData["rbputovanja"].ToString() : string.Empty;
                putovanje.datum = putovanjeData["datum"] != null ? putovanjeData["datum"].ToString() : string.Empty;
                putovanje.lokacija = putovanjeData["lokacija"] != null ? putovanjeData["lokacija"].ToString() : string.Empty;

            }

            return putovanje;
        }

        public static List<Putovanja> GetPutovanja()
        {
            ISession session = SessionManager.GetSession();
            List<Putovanja> putovanja = new List<Putovanja>();


            if (session == null)
                return null;

            var putovanjaData = session.Execute("select * from \"Putovanja\"");


            foreach (var putovanjeData in putovanjaData)
            {
                Putovanja putovanje = new Putovanja();
                putovanje.rbputovanja = putovanjeData["rbputovanja"] != null ? putovanjeData["rbputovanja"].ToString() : string.Empty;
                putovanje.datum = putovanjeData["datum"] != null ? putovanjeData["datum"].ToString() : string.Empty;
                putovanje.lokacija = putovanjeData["lokacija"] != null ? putovanjeData["lokacija"].ToString() : string.Empty;

                putovanja.Add(putovanje);
            }



            return putovanja;
        }

        public  void AddPutovanja(string rbputovanja, string datum, string lokacija)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet putovanjeData = session.Execute("insert into \"Putovanja\" (\"rbputovanja\", datum,lokacija )   values ('" + rbputovanja + "', '" + datum + "', '" + lokacija + " ')");
        }

        #endregion



        #region Uspomene
        public static Uspomene GetUspomenu(string rbuspomene)
        {
            ISession session = SessionManager.GetSession();
            Uspomene uspomena = new Uspomene();

            if (session == null)
                return null;

            Row uspomenaData = session.Execute("select * from \"Uspomene\" where \"rbuspomene\"='1'").FirstOrDefault();

            if (uspomenaData != null)
            {
                uspomena.rbuspomene = uspomenaData["rbuspomene"] != null ? uspomenaData["rbuspomene"].ToString() : string.Empty;
                uspomena.datum = uspomenaData["datum"] != null ? uspomenaData["datum"].ToString() : string.Empty;
                uspomena.opis = uspomenaData["opis"] != null ? uspomenaData["opis"].ToString() : string.Empty;
            }

            return uspomena;
        }

        public List<Uspomene> GetUspomene()
        {
            ISession session = SessionManager.GetSession();
            List<Uspomene> uspomene = new List<Uspomene>();


            if (session == null)
                return null;

            var uspomeneData = session.Execute("select * from \"Uspomene\"");


            foreach (var uspomenaData in uspomeneData)
            {
                Uspomene uspomena = new Uspomene();
                uspomena.rbuspomene = uspomenaData["rbuspomene"] != null ? uspomenaData["rbuspomene"].ToString() : string.Empty;
                uspomena.datum = uspomenaData["datum"] != null ? uspomenaData["datum"].ToString() : string.Empty;
                uspomena.opis = uspomenaData["opis"] != null ? uspomenaData["opis"].ToString() : string.Empty;
              
                uspomene.Add(uspomena);
            }



            return uspomene;
        }

        public  void AddUspomenu(string rbuspomene, string datum, string opis)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet uspomeneData = session.Execute("insert into \"Uspomene\" (\"rbuspomene\", datum, opis)  values ('" + rbuspomene + "', '" + datum + "', '" + opis + "')");

        }


        #endregion

        #region Fakultet
        public static Fakultet GetFakultet(string rbobaveze)
        {
            ISession session = SessionManager.GetSession();
            Fakultet fakultet = new Fakultet();

            if (session == null)
                return null;

            Row fakultetData = session.Execute("select * from \"Fakultet\" where \"rbobaveze\"='1'").FirstOrDefault();

            if (fakultetData != null)
            {
                fakultet.rbobaveze = fakultetData["rbobaveze"] != null ? fakultetData["rbobaveze"].ToString() : string.Empty;
                fakultet.datum = fakultetData["datum"] != null ? fakultetData["datum"].ToString() : string.Empty;
                fakultet.opis = fakultetData["opis"] != null ? fakultetData["opis"].ToString() : string.Empty;
                fakultet.uradjen = fakultetData["uradjen"] != null ? fakultetData["uradjen"].ToString() : string.Empty;

            }

            return fakultet;
        }

        public List<Fakultet> GetFakulteti()
        {
            ISession session = SessionManager.GetSession();
            List<Fakultet> fakulteti = new List<Fakultet>();


            if (session == null)
                return null;

            var fakultetiData = session.Execute("select * from \"Fakultet\"");


            foreach (var fakultetData in fakultetiData)
            {
                Fakultet fakultet = new Fakultet();
                fakultet.rbobaveze = fakultetData["rbobaveze"] != null ? fakultetData["rbobaveze"].ToString() : string.Empty;
                fakultet.datum = fakultetData["datum"] != null ? fakultetData["datum"].ToString() : string.Empty;
                fakultet.opis = fakultetData["opis"] != null ? fakultetData["opis"].ToString() : string.Empty;
                fakultet.uradjen = fakultetData["uradjen"] != null ? fakultetData["uradjen"].ToString() : string.Empty;

                fakulteti.Add(fakultet);
            }



            return fakulteti;
        }

        public void AddFakultet(string rbobaveze, string datum, string opis, string uradjen)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            RowSet fakultetData = session.Execute("insert into \"Fakultet\" (\"rbobaveze\", datum, opis, uradjen)   values ('" + rbobaveze + "', '" + datum + "', '" + opis + "', '" + uradjen + "')");

        }

        public void DeleteFakultet(string rbobaveze)
        {
            ISession session = SessionManager.GetSession();
            Fakultet fakultet = new Fakultet();

            if (session == null)
                return;

            RowSet fakultetData = session.Execute("delete from \"Fakultet\" where \"rbobaveze\" = '" + rbobaveze + "'");

        }

        public void UpdateFakultet(string rbobaveze, string noviOpis)
        {
            ISession session = SessionManager.GetSession();
            Fakultet fakultet = new Fakultet();

            if (session == null)
                return;

            RowSet FfakultetData = session.Execute("update \"Fakultet\" SET \"opis\" ='" + noviOpis + "' where \"rbobaveze\" = '" + rbobaveze + "'");

        }

        #endregion

        #region Aktivnosti
        public Aktivnosti GetAktivnost(string rbaktivnosti)
        {
            ISession session = SessionManager.GetSession();
            Aktivnosti aktivnost= new Aktivnosti();

            if (session == null)
                return null;

            Row aktivnostData = session.Execute("select * from \"Aktivnosti\" where \"rbaktivnosti\"='" + rbaktivnosti + "'").FirstOrDefault();

            if (aktivnostData != null)
            {
                aktivnost.rbaktivnosti = aktivnostData["rbaktivnosti"] != null ? aktivnostData["rbaktivnosti"].ToString() : string.Empty;
                aktivnost.datum = aktivnostData["datum"] != null ? aktivnostData["datum"].ToString() : string.Empty;
                aktivnost.naziv = aktivnostData["naziv"] != null ? aktivnostData["naziv"].ToString() : string.Empty;
                aktivnost.opis = aktivnostData["opis"] != null ? aktivnostData["opis"].ToString() : string.Empty;

            }

            return aktivnost;
        }

        public List<Aktivnosti> GetAktivnosti()
        {
            ISession session = SessionManager.GetSession();
            List<Aktivnosti> aktivnosti = new List<Aktivnosti>();


            if (session == null)
                return null;

            var aktivnostiData = session.Execute("select * from \"Aktivnosti\"");


            foreach (var aktivnostData in aktivnostiData)
            {
                Aktivnosti aktivnost = new Aktivnosti();
                aktivnost.rbaktivnosti = aktivnostData["rbaktivnosti"] != null ? aktivnostData["rbaktivnosti"].ToString() : string.Empty;
                aktivnost.datum = aktivnostData["datum"] != null ? aktivnostData["datum"].ToString() : string.Empty;
                aktivnost.naziv = aktivnostData["naziv"] != null ? aktivnostData["naziv"].ToString() : string.Empty;
                aktivnost.opis = aktivnostData["opis"] != null ? aktivnostData["opis"].ToString() : string.Empty;

                aktivnosti.Add(aktivnost);
            }



            return aktivnosti;
        }

        public void AddAktivnost(string rbaktivnosti, string datum, string naziv, string opis)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;


            RowSet aktivnostData = session.Execute("insert into \"Aktivnosti\" (\"rbaktivnosti\", datum, naziv, opis)  values ('" + rbaktivnosti + "', '" + datum + "', '" + naziv + "', '" + opis + "')");

        }

        public void DeleteAktivnost(string rbaktivnosti)
        {
            ISession session = SessionManager.GetSession();
            Aktivnosti aktivnost = new Aktivnosti();

            if (session == null)
                return;

            RowSet aktivnostData = session.Execute("delete from \"Aktivnosti\" where \"rbaktivnosti\" = '" + rbaktivnosti + "'");

        }

        public void UpdateAktivnost(string rbaktivnosti, string noviOpis)
        {
            ISession session = SessionManager.GetSession();
            Aktivnosti aktivnost = new Aktivnosti();

            if (session == null)
                return;

            RowSet aktivnostData = session.Execute("update \"Aktivnosti\" SET \"opis\" ='" + noviOpis + "' where \"rbaktivnosti\" = '" + rbaktivnosti + "'");

        }

        #endregion
    }
}
