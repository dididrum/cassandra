﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraDataLayer.QueryEntities
{
    public class Fakultet
    {
        public string rbobaveze { get; set; }
        public string datum { get; set; }
        public string opis { get; set; }
        public string uradjen { get; set; }
    }
}
