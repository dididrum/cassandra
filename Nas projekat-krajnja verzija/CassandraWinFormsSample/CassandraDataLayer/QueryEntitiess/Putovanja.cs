﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraDataLayer.QueryEntities
{
    public class Putovanja
    {
        public string rbputovanja { get; set; }
        public string datum { get; set; }
        public string lokacija { get; set; }
    }
}
