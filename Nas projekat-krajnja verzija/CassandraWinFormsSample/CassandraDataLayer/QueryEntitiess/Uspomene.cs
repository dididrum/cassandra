﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraDataLayer.QueryEntities
{
    public class Uspomene
    {
        public string rbuspomene { get; set; }
        public string datum { get; set; }
        public string opis { get; set; }
    }
}
