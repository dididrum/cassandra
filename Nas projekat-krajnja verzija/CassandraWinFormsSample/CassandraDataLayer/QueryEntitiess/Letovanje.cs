﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CassandraDataLayer.QueryEntities
{
    public class Letovanje
    {
        public string rbletovanja { get; set; }
        public string datum { get; set; }
        public string lokacija { get; set; }
    }
}
